<?php $contacts = $params['company']['contacts'];?>
<div id="<?=$data['id']?>" class="<?=$data['class']?>">


    <div class="<?=$data['adresses']['class']?>">
        <i aria-hidden="true" class="text-primary fa fa-<?=$data['adresses']['icon']?> px-2 p-1 small"></i>
        <b><a class="text-primary pointer" data-toggle="modal" data-target="#map_modal">Мы находимся здесь!</a></b>
        <?php if ($contacts['work_time']['text']):?>
            <b><a class="d-block small text-dark"><?=$contacts['work_time']['text']?></a></b>
        <?php endif;?>
        <ul class="list-inline small m-0" id="nav-adress">

            <?php foreach($contacts['adress'] as $key => $adress):?>
                <li class="">
                    <a class="text-secondary <?=count($contacts['adress']) > 1 ? 'small' : '' ?>">
                        <?= ($adress['city'] != $params['settings']['default_city'] ? 'г. ' . $adress['city'] . ', ' : '') . 'ул. '. $adress['street'] .' '. $adress['home'] . ($adress['description'] ? ('<i class="d-none d-lg-inline">' .', '. $adress['description']. '</i> ') : '') . ($adress['floor'] ? ', ' . $adress['floor'] . ' этаж' : '');?>
                    </a>
                    <?php if ($adress['phone']):?>
                    <a class=" mb-2 text-secondary <?=count($contacts['adress']) > 1 ? 'small' : '' ?>" title="<?=$data['phones']['href_title']?>" href="tel:<?='+7' . substr(preg_replace('/[^0-9]/', '', $adress['phone']),1)?>;">
                        <i aria-hidden="true" class="text-primary fa fa-phone px-2 p-1"></i>
                        <?=$adress['phone'] . ($key+1 != count($contacts['adress']) ? ';' : '')?>
                    </a>
                    <?php endif;?>
                </li>

            <?php endforeach;?>
        </ul>
    </div>
</div>