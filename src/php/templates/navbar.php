<nav id="<?$data['container_id']?>" class="navbar navbar-expand-lg navbar-light bg-light sticky-top p-0 <?$data['container_class']?>">

    <?php if($params['settings']['navbar_collapse']):?>
        <!-- Toggler/collapsible Button -->
        <button type="button" data-toggle="collapse" data-target="#<?$data['id']?>" aria-expanded="false" class="navbar-toggler btn btn-primary py-3 ml-1 mr-3 order-2 col text-primary collapsed">
            <span>Меню</span>
        </button>
    <?php endif;?>

    <!-- Navbar links -->
    <div id="<?$data['id']?>" class="<?=$params['settings']['navbar_collapse'] ? 'collapse navbar-collapse' : ''?> text-center">
        <ul class="navbar-nav col">
            <!-- Brand -->
            <?php if($params['settings']['logo_in_navbar'] && $params['company']['logo']):?>
                <li class="nav-item col-2">
                    <?php Constructor::create_block('logo');?>
                </li>
            <?php endif;?>

            <?php foreach($data['links'] as $href => $name):?>
                <li class="nav-item col">
                    <a class="nav-link col align-middle" href="#<?=$href?>"><?=$name?></a>
                </li>
            <?php endforeach;?>

            <?php if ($params['settings']['adress_in_navbar'] && $contacts['adress']){
                Constructor::create_block('contacts');
            };?>
        </ul>
    </div>
</nav>