<div id="<?=$data['id']?>_modal" class="modal <?=$data['class']?>" tabindex="-1" role="dialog">
    <div class="modal-dialog <?=$data['large'] ? 'modal-lg' : ''?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?=$data['title']?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php foreach($data['blocks'] as $block):?>
                    <div class="<?=$block['wrap_class'] ? $block['wrap_class'] : $data['default_wrap_class'] ?>">
                        <?php if ($block['is_form']){
                            echo \app\Constructor::create_form($block['type']);
                        }else{
                            echo \app\Constructor::create_block($block['type'], $block['content']);
                        }?>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>