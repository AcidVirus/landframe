<div class="card card-block p-3 opacity-95 h-100 shadow">
    <div id="client_form" class="container p-3">
        <?=$data['header'] ? '<h5 class="text-primary">'.$data['header'].'</h5>' : ''?>
        <?=$data['text'] ? '<span class="text-dark">'.$data['text'].'</span>' : ''?>
        <form id="<?=$data['id']?>" class="<?=$data['class']?>">
        <?php foreach ($data['fields'] as $num => $field):?>
            <div class="form-group" <?=($field == 'secure' ? 'style="display:none"' : '')?>>
                <label for="<?=$fields[$field]['id']?>">
                    <?php if($fields[$field]['required']):?>
                    <a class="text-danger">*</a>
                    <?php endif;?>
                    <?=$fields[$field]['label']?>:
                    <a class="text-danger" id="<?=$fields[$field]['id']?>_l"></a>
                </label>
                <input <?=$fields[$field]['required'] ? 'required' : ''?> <?=$fields[$field]['value'] ? 'value="' . $fields[$field]['value'] .'"' : ''?> data-field-type="<?=$fields[$field]['data-field-type']?>" type="text" class="form-control" id="<?=$fields[$field]['id']?>" placeholder="<?=$fields[$field]['placeholder']?>">
            </div>
        <?php endforeach;?>

        <p class="policy_text text-muted text-center mt-2">Продолжая, вы даете согласие на обработку своих персональных данных и соглашаетесь с условиями <a href="files/policy.pdf">Пользовательского соглашения</a></p>
        <input id="client_send" class="mt-3 btn btn-block btn-primary text-white" type="submit" value="ПОЛУЧИТЬ ПРАЙС">
        </form>
    </div>
</div>
