<div class="<?=$data['class']?>" id="<?=$data['id']?>">
    <?=\app\Constructor::create_block('block_header', ['text' => $data['header'], 'class' => $data['header-class']])?>
    <?php foreach($data['content'] as $item):?>
        <?php switch ($item['type']):?>
<?php case 'header':?>
            <h5><b><?=$item['text']?></b></h5>
        <? break; ?>
        <?php case 'list':?>
            <?=\app\Constructor::create_block('list', ['header' => $item['header'], 'items' => $item['items']])?>
        <? break; ?>
        <?php case 'text':?>
            <p><?=$item['text']?></p>
        <? break; ?>
        <?php endswitch ?>
    <?php endforeach;?>
</div>