<div id="categories">
    <div class="container">
        <?=\app\Constructor::create_block('block_header', ['text' => $data['header']])?>
        <div class="row text-center pl-3 pb-4 <?=$data['class']?>">
            <?php foreach($data['items'] as $id => $item):?>
                <?=\app\Constructor::create_block('link', [
                        'class' => $item['class'],
                        'icon' => $item['icon'],
                        'text' => $item['text'],
                        'href' => $item['href'],
                ])?>
            <?php endforeach;?>
        </div>
    </div>
</div>