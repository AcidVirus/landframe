<div id="advantages">
    <div class="container">
        <?=\app\Constructor::create_block('block_header', ['text' => $data['header']])?>
        <div class="row p-2 p-md-5 text-center <?=$data['class']?>">
            <?php foreach($data['items'] as $id => $item):?>
                <div class="col-6 col-md-3 px-1 px-md-3">
                    <i class="fa fa-<?=$item['icon']?>"></i>
                    <h6 class="pt-3"><?=$item['text']?></h6>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>