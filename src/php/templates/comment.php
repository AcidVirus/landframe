<div class="card col-12 col-md-6">
    <div class="col-4">
        <img class="rounded" src="<?=$data['icon']?>" alt="<?=$data['name']?>" title="<?=$data['name']?>">
    </div>
    <div class="col-8">
        <b><h4 class="text-primary pb-3"><?=$data['text']?></h4></b>
        <p class="text-justify"><?=$data['text']?></p>
    </div>
</div>