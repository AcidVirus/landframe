<div class="<?=$data['class']?>" id="<?=$data['id']?>">
    <div class="container">
        <?=$data['text']?>
        <div class="row">
            <?php \app\Constructor::create_block('logo_text', [
                'main-text' => 'ТД "Система Безопасности"',
                'sub-text' => 'комплексные поставки оборудования безопасности',
            ]);?>
            <?php \app\Constructor::create_block('contacts')?>
        </div>
    </div>

    <div class="copyright text-center text-black-50 my-3">© <?=$params['company']['name'] . ' ' . date('Y')?></div>
</div>