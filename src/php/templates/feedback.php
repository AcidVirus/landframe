<div id="feedback">
    <div class="container">
        <?=\app\Constructor::create_block('block_header', ['text' => $data['header']])?>
        <div class="row text-center p-3 pb-4 <?=$data['class']?>">
            <?php foreach($data['items'] as $id => $comment):?>
                <?=\app\Constructor::create_block('comment', [
                        'class' => $comment['class'],
                        'icon' => $comment['icon'],
                        'name' => $comment['name'],
                        'text' => $comment['text'],
                ])?>
            <?php endforeach;?>
        </div>
    </div>
</div>