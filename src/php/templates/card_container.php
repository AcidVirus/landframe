<div id="<?=$data['id']?>" class="container mb-3 <?=$data['class']?>">
    <div class="row">
        <?php foreach($data['blocks'] as $block):?>
            <div class="<?=$block['wrap_class'] ? $block['wrap_class'] : $data['default_wrap_class'] ?>">
                <?php if ($block['is_form']){
                    echo \app\Constructor::create_form($block['type'], $block['content']);
                }else{
                    echo \app\Constructor::create_block($block['type'], $block['content']);
                }?>
            </div>
        <?php endforeach;?>
    </div>
</div>