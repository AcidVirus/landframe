<div class="logo <?=$data['class']?>" id="<?=$data['id']?>">
    <a class="text-decoration-none" href="#">
        <p class="<?=$data['main-text-class']?>"><?=$data['main-text']?></p>
        <p class="<?=$data['sub-text-class']?>"><?=$data['sub-text']?></p>
    </a>
</div>