<?php
    $company = $params['company'];
?>
<div class="d-none">
    <div itemscope itemtype="https://schema.org/Organization">
        <span itemprop="name"><?=$company['name']?></span>
        <?php foreach($company['contacts']['adress'] as $adress):?>
            <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                <meta itemprop="addressLocality" content="<?=$adress['city']?>, Россия" />
                <meta itemprop="postalCode" content="<?=$adress['postal_code']?>" />
                <meta itemprop="streetAddress" content="Ул. <?=$adress['street']?> <?=$adress['home']?>" />
                <meta itemprop="telephone" content="<?=$adress['phone']?>" />
            </div>
        <?php endforeach;?>
        <img itemprop="logo" src="<?=$company['logo']?>" />
        Электронная почта: <span itemprop="email"><?=$company['contacts']['email']?></span>
        Сайт: <span itemprop="url"><?=$company['contacts']['url']?></span>
    </div>
</div>