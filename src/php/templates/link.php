<a class="col-5 card col-md-2 m-2 p-md-3 m-md-3 <?=$data['class']?>" <?=$data['href'] ? 'href="' . $data['href'] . '"' : ''?>>
    <img src="<?=$data['icon']?>" alt="<?=$data['text']?>" title="<?=$data['text']?>">
    <h6 class="pt-3"><?=$data['text']?></h6>
</a>