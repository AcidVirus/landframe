<?php
return [
    'navbar' => [
        //href => Title
    ],
    'main_text' => [
        [
            'type' => 'text',
            'text' => 'ООО "ТОРГОВЫЙ ДОМ "СИСТЕМА БЕЗОПАСНОСТИ" - один из крупнейших поставщиков систем безопасности в России. Мы работаем напрямую с зарекомендовавшими себя мировыми и российскими брендами (Tantos, RVI, Optimus, CTV, ITECH, Falcon, VIZIT, Hikvision, Activecam, Metakom, Ростов-Дон, Бастион, Bewerd, Samsung, Novicam, Sigur, HiWatch, Dahua).На все выполненные нашей компанией работы предоставляется гарантия.',
        ],
        [
            'type' => 'text',
            'text' => 'Компания создана профессиональными инженерами и опытными маркетологами – мы прекрасно разбираемся в продаваемой продукции и внимательно следим за всеми инновациями в области систем безопасности, оперативно обновляя ассортимент товаров в соответствии с последними разработками.',
        ],
        [
            'type' => 'text',
            'text' => 'Широкий ассортимент: от домофона и систем охранного видео наблюдения до кабельной продукции и пожарного оборудования.',
        ],
        [
            'type' => 'list',
            'header' => 'Разнообразный спектр услуг:',
            'items' => [
                'оптово-розничная продажа оборудования,',
                'индивидуальное проектирование, монтаж и запуск комплексных систем безопасности «под ключ»,',
                'выполнение монтажных работ,',
                'техническое обслуживание и ремонт систем безопасности.',
            ]
        ],
        [
            'type' => 'text',
            'text' => 'Индивидуальная система скидок позволит подобрать для вас оптимальное решение по привлекательной цене.',
        ],
    ],
    'advantages' => [
        [
            'class' => '',
            'icon' => 'shield text-light text-center bg-primary circle adv-icon h1',
            'text' => 'Гарантия 1,5 года',
        ],
        [
            'class' => '',
            'icon' => 'lock text-light text-center bg-primary circle adv-icon h1',
            'text' => 'Монтаж и установка под ключ',
        ],
        [
            'class' => '',
            'icon' => 'bolt text-light text-center bg-primary circle adv-icon h1',
            'text' => 'Самое оперативное обслуживание в городе',
        ],
        [
            'class' => '',
            'icon' => 'gift text-light text-center bg-primary circle adv-icon h1',
            'text' => 'Подарки каждому клиенту',
        ],
    ],
    'about_text' => [
        [
            'type' => 'text',
            'text' => 'В данном магазине вы можете оформить и оплатить заказ выбранного вами оборудования. При необходимости вас проконсультируют квалифицированные менеджеры, с большим опытом работы в данной сфере. Так же наша организация выполняет услуги по установке оборудования представленного на нашем сайте. При покупке товара в нашем магазине вы получаете полный спектр услуг, начиная с подбора и заканчивая монтажом приобретенного оборудования. Мы будем рады видеть вас в любое рабочее время с 10:00 до 20:00 с понедельника по пятницу, суббота; воскресенье выходной.',
        ],
        [
            'type' => 'header',
            'text' => 'НАШИ СИСТЕМЫ НА СТРАЖЕ ВАШЕЙ БЕЗОПАСНОСТИ!',
        ],
        [
            'type' => 'text',
            'text' => 'Безопасность является одной из главных потребностей человека. Ее обеспечение – это такая же насущная необходимость, как потребность в пище или одежде. Ведь только чувствуя себя защищенными, мы можем спокойно работать, растить детей и радоваться жизни. Решить проблему безопасности вашего дома, квартиры, офиса, магазина или предприятия поможет ООО "Торговый дом "Система безопасности".',
        ],
        [
            'type' => 'list',
            'header' => 'Первоочередная задача любого человека, любой семьи – обеспечение безопасности своего жилища. И здесь мы предлагаем массу вариантов. Это может быть:',
            'items' => [
                'Установка в подъезде обычного домофона;',
                'Монтаж видео домофона, который позволяет видеть гостя и даже переадресовывает звонок пришедшего на ваш телефон;',
                'Установка GSM сигнализации, оповещающей СМС сообщениями о любых необычных ситуациях в вашем доме, вплоть до отключения электричества и изменения температуры в комнате;',
                'Установка видеонаблюдения – как в подъездах, так и в квартирах. Особенно актуальна для семей, где есть дети. С помощью цифрового наблюдения в режиме реального времени вы всегда будете в курсе, чем заняты дети в ваше отсутствие, либо сможете контролировать действия няни, сидящей с вашим ребенком;',
                'Установка шлагбаума на въезде на придомовую территорию, который ограничит доступ во двор посторонним автомобилям.',
            ]
        ],
        [
            'type' => 'header',
            'text' => 'БЕЗОПАСНОСТЬ БИЗНЕСА',
        ],
        [
            'type' => 'list',
            'header' => 'Защищенность сотрудников и внутренней информации – одно из слагаемых успеха эффективной работы компании. Наши специалисты помогут подобрать и спроектировать комплексную систему безопасности вашего предприятия, в том числе:',
            'items' => [
                'Установку шлагбаумов, турникетов, ограждений проходной зоны КПП;',
                'Монтаж видеонаблюдения в лифтах, офисах, а также наружных камер;',
                'Установку систем контроля доступа в помещения;',
                'Установку антикражных ворот в магазинах, предупреждающего о попытке похищения товара;',
                'Установку парковочного оборудования, гарантирующее сохранность ваших парковочных мест и защиту автомобилей;',
                'Охранно-пожарное оборудование.',
            ]
        ],
    ],

    'categories' => [
        [
            'class' => '',
            'icon' => 'img/links/camera.png',
            'text' => 'Видеонаблюдение',
            'href' => 'http://uralvision.com/videonablyudenie',
        ],
        [
            'class' => '',
            'icon' => 'img/links/skud.png',
            'text' => 'Антикражные системы',
            'href' => 'http://uralvision.com/antikrazhnyye-sistemy',
        ],
        [
            'class' => '',
            'icon' => 'img/links/domophone.png',
            'text' => 'Домофоны и средства связи',
            'href' => 'http://uralvision.com/domofony',
        ],
        [
            'class' => '',
            'icon' => 'img/links/acc_control.png',
            'text' => 'Системы доступа',
            'href' => 'http://uralvision.com/kontrol-dostupa',
        ],
        [
            'class' => '',
            'icon' => 'img/links/interactive.png',
            'text' => 'Интерактивное оборудование',
            'href' => 'http://uralvision.com/interaktivnoye-oborudovaniye',
        ],
        [
            'class' => '',
            'icon' => 'img/links/battery.png',
            'text' => 'Источинки питания',
            'href' => 'http://uralvision.com/istochniki-pitaniya',
        ],
        [
            'class' => '',
            'icon' => 'img/links/horn.png',
            'text' => 'Системы оповещения',
            'href' => 'http://uralvision.com/sistemy-opoveshcheniya-i-muzykalnoy-translyatsii',
        ],
        [
            'class' => '',
            'icon' => 'img/links/ops.png',
            'text' => 'Оборудование ОПС',
            'href' => 'http://uralvision.com/ops',
        ],
        [
            'class' => '',
            'icon' => 'img/links/gsm_alert.png',
            'text' => 'GSM сигнализациии',
            'href' => 'http://uralvision.com/okhrannaya-signalizatsiya',
        ],
        [
            'class' => '',
            'icon' => 'img/links/smart_house.png',
            'text' => 'Умный дом',
            'href' => 'http://uralvision.com/umnyy-dom',
        ],
        [
            'class' => '',
            'icon' => 'img/links/siz.png',
            'text' => 'СИЗ',
            'href' => 'http://uralvision.com/sredstva-individualnoy-zashchity-siz',
        ],
        [
            'class' => '',
            'icon' => 'img/links/explode.png',
            'text' => 'Взрывозащищенное оборудование',
            'href' => 'http://uralvision.com/vzryvozashchishchennoye-oborudovaniye',
        ],
        [
            'class' => '',
            'icon' => 'img/links/cabble.png',
            'text' => 'Кабельная продукция',
            'href' => 'http://uralvision.com/kabel',
        ],
        [
            'class' => '',
            'icon' => 'img/links/parking.png',
            'text' => 'Парковочное оборудование',
            'href' => 'http://uralvision.com/parkovochnoye-oborudovaniye',
        ],
        [
            'class' => '',
            'icon' => 'img/links/mount.png',
            'text' => 'Монтажные материалы',
            'href' => 'http://uralvision.com/montazhnyye-materialy',
        ],
    ],
    'brands' => [
        [
            'class' => '',
            'icon' => 'img/brands/vizit.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/came.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/activision.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/beward.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/ctv.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/dahua.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/domination.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/everfocus.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/bastion.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/aksilium.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/doorhan.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/bolid.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/ekskon.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/eternis.jpg',
            'href' => '',
            'text' => '',
        ],
        [
            'class' => '',
            'icon' => 'img/brands/etra-spetsavtomatika.jpg',
            'href' => '',
            'text' => '',
        ],
    ],

    'feedback' => [
        [
            'icon' => '',
            'name' => 'Иван И.И.',
            'text' => 'Отзыв космос',
        ],
        [
            'icon' => '',
            'name' => 'Иван И.И.',
            'text' => 'Отзыв космос',
        ],
        [
            'icon' => '',
            'name' => 'Иван И.И.',
            'text' => 'Отзыв космос',
        ],
        [
            'icon' => '',
            'name' => 'Иван И.И.',
            'text' => 'Отзыв космос',
        ],
    ],
];