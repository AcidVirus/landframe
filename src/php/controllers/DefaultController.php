<?php
use app\Mailer;

function main()
{
    require_once __DIR__ . '/../classes/mailer.php';
    $params = require __DIR__ . '/../params.php';

    $data = $_POST['data'];

    if ($data['password']) {
        return false;
    }

    switch ($_POST['action']) {
        case 'mailSend':
            Mailer::telegram_send($params['settings']['telegram_bot'], $data['name'], $data['phone'], $data['email'], $data['other_info']);
            $res = Mailer::mail_send($params['company']['contacts']['email'], $data['name'], $data['phone'], $data['email'], $data['other_info']);
            break;
        default:
            $res = false;
            break;
    }
    return $res;
}

echo main();