<?php
return [
    'settings' => [
        'need_spy' => false,
        'need_btn_up' => true,

        'navbar_collapse' => false,

        'logo_in_navbar' => false,
        'logo_in_top' => true,

        'adress_in_navbar' => false,
        'adress_in_top' => true,

        'default_city' => 'Пермь',
        'telegram_bot' => [
            //что бы узнать id чатов - https://api.telegram.org/bot{bot_ID}/getUpdates
            'id' => '864346796:AAG58r7SaLg7aTkRj3zqH0X7iCzWIxoLiVY',
            'user_chats' => [
                '877677485',
                '630084974',
            ]
        ]
    ],
    'meta' => [
        'title' => 'ТД "Система Безопасности"',
        'description' => '',
        'tags' => [
            /*meta tags format
            *'name' => 'content',
            */
            'robots' => 'nofollow',
            'yandex-verification' => '8d85aaa65967083c',
        ],
        'metrics' => [
            //full dom element
            //'<meta http-equiv="x-ua-compatible" content="ie=edge">',
        ],
    ],
    'company' => [
        'name' => 'ТД "Система Безопасности"',
        'logo' => 'img/logo.png',
        'preview' => '',
        'contacts' => [
            'work_time' => [
                'text' => 'Пн-пт 09:00-18:00',
                'micro' => 'Mo-Fr 09:00-18:00',
            ],
            'email' => 'cctv.perm@yandex.ru',
            'url' => 'uralvision.com',
            'adress' => [
                [
                    'city' => 'Пермь',
                    'postal_code' => '614068',
                    'street' => 'Ленина',
                    'home' => '76',
                    'floor' => 3,
                    'description' => 'ТЦ Бизнес Галереи',
                    'phone' => '8 (342) 256-34-35',
                ],
                [
                    'city' => 'Пермь',
                    'postal_code' => '614007',
                    'street' => 'М. Горького',
                    'home' => '75',
                    'floor' => 1,
                    'description' => '',
                    'phone' => '8 (342) 256-34-35',
                ],
                [
                    'city' => 'Пермь',
                    'postal_code' => '614101',
                    'street' => 'Ласьвинская',
                    'home' => '19',
                    'floor' => 2,
                    'description' => 'ТЦ Охотный Ряд',
                    'phone' => '8 (922) 243-76-24',
                ],
                [
                    'city' => 'Москва',
                    'postal_code' => '614101',
                    'street' => 'Кондратюка',
                    'home' => '9',
                    'floor' => '',
                    'description' => '',
                    'phone' => '',
                ],
            ]
        ],
        'map' => '
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A22336b919c4e87a3ee595510fddd15548c0eae42b13e1ae709048cf8d502d9d8&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
        ',
    ],
];