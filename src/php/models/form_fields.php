<?php
return [
    //secure - поле от спама.
    'secure' =>[
        'type' => 'password',
        'id' => 'form-field-password',
        'class' => 'password',
        'required' => true,
        'placeholder' => 'Пароль',
        'value' => '',
        'data-field-type' => 'password',
    ],
    'email' => [
        'type' => 'text',
        'id' => 'form-field-email',
        'class' => '',
        'required' => true,
        'placeholder' => 'E-mail',
        'label' => 'E-mail',
        'data-field-type' => 'email',
    ],
    'fio' => [
        'type' => 'text',
        'id' => 'form-field-fio',
        'class' => '',
        'required' => true,
        'placeholder' => 'Ф.И.О.',
        'label' => 'Ф.И.О.',
        'data-field-type' => 'fio',
    ],
    'phone' => [
        'type' => 'text',
        'id' => 'form-field-phone',
        'class' => '',
        'required' => true,
        'placeholder' => 'Телефон',
        'label' => 'Телефон',
        'data-field-type' => 'phone',
    ],
];