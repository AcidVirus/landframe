<?php
return [
    'logo_text' => [
        'id' => 'header-logo',
        'class' => 'col-12 col-md-4 lead text-center text-sm-small',

        'main-text-class' => 'h1 text-primary font-weight-bold',
        'main-text' => 'Логотип',

        'sub-text-class' => 'logo-sub text-dark text-uppercase small font-weight-bold',
        'sub-text' => 'подпись',
    ],

    'logo' => [
        'id' => 'header-logo',
        'class' => '',
    ],

    'contacts' => [
        'id' => 'header-contacts',
        'class' => 'col-12 col-md-8 pr-0 mr-0',
        'phones' => [
            'class' => 'col text-right text-md-center ml-auto mr-0 lead navbar-text',
            'href_title' => 'Нажмите, что бы позвонить',
            'icon' => 'phone in-circle',
        ],
        'adresses' => [
            'class' => 'col text-center text-md-right ml-auto mr-0 lead navbar-text',
            'icon' => 'map-marker in-circle',
        ]
    ],

    'navbar' => [
        'container_id' => '',
        'container_class' => '',

        'id' => '',
        'class' => '',
        'links' => [],
    ],

    'nav-links' => [
        'id' => '',
        'class' => '',
        'links' => [
            $content['navbar'],
        ],
    ],

    'jumbotron' =>[
        'id' => 'jumbotron',
        'class' => 'jumbotron',

        'header' => 'Наша компания',
        'text' => 'test-jumbotron',

        'img' => [
            'test.png' => 'title',
        ]
    ],

    'advantages' => [
        'id' => 'advantages',
        'class' => '',
        'header' => 'Преимущества',
        'items' => [],
    ],

    'links' => [
        'id' => 'categories',
        'class' => '',
        'header' => 'У нас вы можете найти',
        'items' => [],
    ],

    'galery' => [
        'id' => 'galery-block',
        'class' => 'galery',

        'header' => 'Галерея',
        'text' => 'test-gal',

        'img' => [
            'test.png' => 'title',
            'test1.png' => 'title',
        ]
    ],

    'footer' => [
        'id' => 'footer',
        'class' => 'footer mt-3',
        'text' => '',
    ],

    'text' => [
        'class' => '',
        'id' => '',
        'header' => '',
        'header-class' => '',
        'content' => [],
    ],

    'feedback' => [
        'id' => '',
        'class' => '',
        'header' => '',
        'items' => [],
    ],

    //______SECONDARY BLOCKS_________
    'comment' => [
        'id' => '',
        'class' => '',
        'icon' => '',
        'name' => '',
        'text' => '',
    ],

    'link' => [
        'class' => '',
        'icon' => '',
        'text' => '',
        'href' => '#',
    ],

    'list' => [
        'id' => '',
        'class' => '',
        'header' => '',
        'items' => [],
    ],

    'block_header' => [
        'text' => '',
        'class' => '',
    ],

    'card_container' => [
        'id' => '',
        'class' => '',
        'default_wrap_class' => 'col-12 bg-white p-3 card shadow',
        'blocks' => [
            'wrap_class' => '',
            'type' => '',
            'is_form' => false,
            'content' => '',
        ],
    ],

    'modal_container' => [
        'id' => '',
        'class' => '',
        'large' => false,
        'title' => '',
        'default_wrap_class' => 'col-12',
        'blocks' => [
            'wrap_class' => '',
            'type' => '',
            'is_form' => false,
            'content' => '',
        ],
    ],

    //______SPECIAL BLOCKS_________
    'seo',
    'map' => [
        'id' => '',
        'class' => '',
        'link' => '',
    ],
];