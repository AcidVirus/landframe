<?php
return [
    'main_form' =>[
        'id' => '',
        'class' => '',

        'header' => '',
        'text' => '',

        'fields' => [
            'fio',
            'phone',
            'email',
            'secure',
        ]
    ],
    'main_form_horizontal' =>[
        'id' => '',
        'class' => '',

        'header' => '',
        'text' => '',

        'fields' => [
            'fio',
            'phone',
            'email',
            'secure',
        ]
    ],
];