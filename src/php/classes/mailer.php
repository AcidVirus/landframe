<?php
namespace app;

class Mailer
{
    public static function mail_send($to = null, $name = null, $phone = null, $email = null, $other_info = null){
        $from = 'client@' . $_SERVER['HTTP_HOST'];
        $subject = 'Заявка';
        $message .= $name ? "\r\nИмя - ".$name : '';
        $message .= $phone ? "\r\nТелефон - <a href=tel:+7" . substr(preg_replace('/[^0-9]/', '', $phone),1) . ">" . $phone . "</a>" : '';
        $message .= $email ? "\r\nE-mail - ".$email : '';
        $message .= $other_info ? "\r\n".$other_info : '';

        $headers = "From: $from\nReply-To: $from\n";
        $headers .= "Content-type: text/plain;\r\n";

        return mail($to, $subject, $message, $headers,"-f{$from}");
    }

    public static function telegram_send($bot_data, $name=null, $phone=null, $email=null, $other_info=null){
        $message = $name ? "\nИмя - ".$name : "";
        $message .= $phone ? "\nТелефон - ". $phone : "";
        $message .= $email ? "\nE-mail - ". $email : "";
        $message = urlencode($message);

        foreach ($bot_data['user_chats'] as $user) {
            file_get_contents('https://api.telegram.org/bot' . $bot_data['id'] . '/sendMessage?chat_id=' . $user . '&text=' . $message);
        }
    }
};
