<?php
namespace app;

class Constructor{
//    private static function set_block_params($block_params, $content){
//        foreach($content as $field => $value) {
//            if (is_array($value)){
//                $block_params[$field] = static::set_block_params($block_params[$field], $value);
//            } elseif (array_key_exists($field, $block_params)) {
//                if (!is_array($block_params[$field])) {
//                    $block_params[$field] = $value;
//                };
//            }
//        }
//        return $block_params;
//    }
    private static function set_block_params($block_params, $content){
        foreach($content as $field => $value) {
//            if ($field == 'block') {
//                static::create_block($value['type'], $value['content']);
//            }else
            if (array_key_exists($field, $block_params)) {
                $block_params[$field] = $value;
            }
        }
        return $block_params;
    }

    public static function create_block($type, $content = []){
        $blocks = require __DIR__ . '/../models/blocks.php';
        $params = require __DIR__ . '/../params.php';

        //Set defaults
        $data = $blocks[$type];

        //Set parameters if given
        if ($content) {
            $data = static::set_block_params($data, $content);
        }

        require (__DIR__.'/../templates/'.$type.'.php');
    }

    public static function create_form($type, $content = []){
        $forms = require __DIR__ . '/../models/forms.php';
        $fields = require __DIR__ . '/../models/form_fields.php';
        $params = require __DIR__ . '/../params.php';
        $data = $forms[$type];
        if ($content) {
            $data = static::set_block_params($data, $content);
        }
        require __DIR__ . '/../templates/forms/' . $type . '.php';
    }

    public static function create_image_link($href, $img){
        return '<a href="' . $href . '"><img src="' . $img . '" alt=""></a>';
    }
};