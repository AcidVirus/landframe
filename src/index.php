<?php
use app\Constructor;
require_once __DIR__ . '/php/classes/constructor.php';

$params = require __DIR__ . '/php/params.php';
$content = require __DIR__ . '/php/content.php';

$description = $params['meta']['title'] . ' - ' . $params['meta']['description'];
$full_url = ($_SERVER['HTTPS'] == 'on' ? 'https://': 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$contacts = $params['company']['contacts'];
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title><?=$params['meta']['title']?></title>
        <meta name="description" content="<?=$params['meta']['description']?>">
        <?php foreach($params['meta']['tags'] as $meta_name => $meta_content):?>
            <meta name="<?=$meta_name?>" content="<?=$meta_content?>">
        <?php endforeach;?>

        <!--OG info begin-->
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="<?=$params['meta']['title']?>">
        <meta property="og:title" content="<?=$params['meta']['title']?>">
        <meta property="og:description" content="<?=$description?>">
        <meta property="og:url" content="<?=$full_url?>">
        <meta property="og:locale" content="ru_RU">
        <?= $params['preview'] ? '<meta property="og:image" content="' . $full_url . $params['company']['preview'] . '" />' : ''?>
        <meta property="og:image:width" content="968">
        <meta property="og:image:height" content="504">

        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        <!--OG info end-->

        <?php foreach($params['meta']['metrics'] as $metric){echo $metric;}?>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,800">
        <link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body data-spy="scroll"  <?=$params['settings']['need_spy'] ? 'data-target=".navbar"' : ''?> data-offset="200">
        <?php if($params['settings']['need_btn_up']):?>
            <div id="button-up" title="На верх" class="text-center bg-primary text-white" style="display: none;" aria-hidden="true"><div class="fa fa-chevron-up"></div></div>
        <?php endif;?>
        <div class="container" id="top-contact">
            <div class="row">
            <?php if($params['settings']['logo_in_top'] && $params['company']['logo']){
                Constructor::create_block('logo_text', [
                        'main-text' => 'ТД "Система Безопасности"',
                        'sub-text' => 'комплексные поставки оборудования безопасности',
                ]);
            }?>

            <?php if ($params['settings']['adress_in_top'] && $contacts['adress']){
                Constructor::create_block('contacts');
            }?>
            </div>
        </div>

        <div class="container p-0">
            <?=Constructor::create_block('navbar', [
                'links' => $content['navbar'],
            ])?>
        </div>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'wrap_class' => 'col-12 col-md-8 card shadow',
                    'type' => 'text',
                    'content' => [
                        'class' => 'p-3 text-justify ',
                        'header' => 'Доверьтесь специалистам!',
                        'header-class' => 'text-left',
                        'content' => $content['main_text']
                    ],
                ],
                [
                    'wrap_class' => 'col-12 col-md-4 mt-3 mt-md-0 pl-0 pl-md-2 pr-0',
                    'type' => 'main_form',
                    'is_form' => true,
                    'content' => [
                        'id' => 'form1',
                        'header' => 'Узнайте об акциях и получите актуальный прайс!',
                    ]
                ],
            ],
        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'advantages',
                    'content' => [
                        'items' => $content['advantages'],
                    ],
                ],
            ],
        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'links',
                    'content' => [
                        'items' => $content['categories'],
                    ]
                ],
            ],
        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'main_form_horizontal',
                    'is_form' => true,
                    'content' => [
                        'id' => 'form2',
                        'header' => 'Узнайте об акциях и получите актуальный прайс!',
                    ]
                ],
            ],
        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'text',
                    'content' => [
                        'class' => 'p-3 text-justify ',
                        'header' => 'Что мы предлагаем',
                        'header-class' => 'text-left',
                        'content' => $content['about_text'],
                    ],
                ],
            ],
        ]);?>

<!--        --><?//=Constructor::create_block('card_container', [
//            'blocks' => [
//                [
//                    'type' => 'feedback',
//                    'content' => [
//                        'class' => 'text-justify ',
//                        'header' => 'Отзывы клиентов',
//                        'items' => $content['feedback'],
//                    ],
//                ],
//            ],
//        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'links',
                    'content' => [
                        'header' => 'Наши партнеры',
                        'items' => $content['brands'],
                    ],
                ],
            ],
        ]);?>

        <?=Constructor::create_block('card_container', [
            'blocks' => [
                [
                    'type' => 'main_form_horizontal',
                    'is_form' => true,
                    'content' => [
                        'id' => 'form3',
                        'header' => 'Узнайте об акциях и получите актуальный прайс!',
                    ]
                ],
            ],
        ]);?>

        <?php Constructor::create_block('footer');?>


        <?=Constructor::create_block('modal_container', [
            'id' => 'thanks',
            'title' => 'Благодарим за заявку!',
            'blocks' => [
                [
                    'type' => 'text',
                    'content' => [
                        'class' => 'p-3 text-justify ',
                        'content' => [
                            [
                                'type' => 'header',
                                'text' => 'Благодарим за оставленую заявку!',
                            ],
                            [
                                'type' => 'text',
                                'text' => 'В ближайшее время с вами свяжется наш специалист!',
                            ]
                        ],
                    ],
                ],
            ],
        ]);?>

        <?=Constructor::create_block('modal_container', [
            'id' => 'error',
            'title' => 'Ошибка',
            'blocks' => [
                [
                    'type' => 'text',
                    'content' => [
                        'class' => 'p-3 text-justify ',
                        'header-class' => 'text-center',
                        'content' => [
                            [
                                'type' => 'header',
                                'text' => 'Возникли технические неполадки :с'
                            ],
                            [
                                'type' => 'text',
                                'text' => 'Приносим свои извинения. Пожалуйста, позвоните по телефону или оставьте заявку позже.'
                            ]
                        ],
                    ],
                ],
            ],
        ]);?>

        <?=Constructor::create_block('modal_container', [
            'id' => 'map',
            'large' => 'true',
            'title' => 'Мы на карте',
            'blocks' => [
                [
                    'type' => 'map',
                    'content' => [
                        'link' => $params['company']['map'],
                    ]
                ],
            ],
        ]);?>

        <?php Constructor::create_block('seo');?>
        <script src="js/main.js"> </script>
    </body>
</html>