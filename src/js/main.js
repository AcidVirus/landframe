//= ../../node_modules/jquery/dist/jquery.js'
//= ../../node_modules/popper.js/dist/umd/popper.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.js'
//= jquery.maskedinput.min.js


function mail_send(context){
    if (!form_validation(context.form)) {
        console.log("Not valid form");
        return false;
    }

    fio = $('#' + context.form.id + ' #form-field-fio').val();
    phone = $('#' + context.form.id + ' #form-field-phone').val();
    email = $('#' + context.form.id + ' #form-field-email').val();
    password = $('#' + context.form.id + ' #form-field-password').val();
    $.post(
        "php/controllers/DefaultController.php",
        {
            action:"mailSend",
            data:{
                name : fio,
                phone : phone,
                email : email,
                password : password,
            }
        },
        function (res) {
            if (res) {
                open_thanks();
                form_clear();
            }else{
                open_error();
            }
        }
    );
}

function form_validation(form){
    is_valid = true;
    $('#' + form.id + ' input').toArray().forEach(function(el){
        is_pass = (el.hasAttribute('data-field-type') && (el.attributes['data-field-type'].value == "password"));
        console.log(el.value + " [" + el.value.length + "] - " + ( is_pass ? el.value.length != 0 : el.value.length == 0));
        if (is_pass ? el.value.length != 0 : el.value.length == 0) {
            is_valid = false;
        }
    });
    return is_valid;
}

function form_clear(form){
    $('#' + form.id + ' input').toArray().forEach(function(el){
        if(el.hasAttribute('data-field-type')){
            el.value = "";
        }
    });
}

function open_thanks(){
    $('#thanks_modal').modal('show');
}


function open_error(){
    $('#error_modal').modal('show');
}

function open_map(){
    $('#map_modal').modal('show');
}

/**
 * Показать кнопку скролла
 */
$(window).scroll(function () {
    //тень блока навигации
    if ($(this).scrollTop() > 10){
        $('nav.sticky-top').addClass('bottom-shadow');
    }else{
        $('nav.sticky-top').removeClass('bottom-shadow');
    }

    // Если отступ сверху больше 200 то показываем кнопку "Наверх"
    if ($(this).scrollTop() > 200) {
        $('#button-up').fadeIn();
    } else {
        $('#button-up').fadeOut();
    }
});

/** Скролл вверх */
$('#button-up').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
    return false;
});

$(function(){
    $('[type="submit"]').on('click', function(e){e.preventDefault(); mail_send(this)});
    $('[data-field-type=phone]').mask('8(999) 999-99-99');
});
