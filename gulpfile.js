"use strict";

/* параметры для gulp-autoprefixer */
var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

/* пути к исходным файлам (src), к готовым файлам (build), а также к тем, за изменениями которых нужно наблюдать (watch) */
var path = {
    build: {
        main_php: 'build/',
        js:    'build/js/',
        css:   'build/css/',
        img:   'build/img/',
        fonts: 'build/fonts/',
        php:   'build/php/',
        files: 'build/files/'
    },
    src: {
        main_php: 'src/*.php',
        js:    'src/js/main.js',
        style: 'src/scss/**/*.scss',
        img:   'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        php:   'src/php/**/*.*',
        files: 'src/files/**/*.*'
    },
    watch: {
        main_php: 'src/*.php',
        js:    'src/js/**/*.js',
        css:   'src/scss/**/*.scss',
        img:   'src/img/**/*.*',
        fonts: 'srs/fonts/**/*.*',
        php:   'src/php/**/*.*',
        files: 'src/files/**/*.*'
    },
    clean:     './build'
};
/* настройки сервера */
var config = {
    proxy: "http://localhost/bs4_gulp/build/",
    // server: {
    //     baseDir: './build'
    // },
    notify: false
};

/* подключаем gulp и плагины */
var gulp = require('gulp'),  // подключаем Gulp
    webserver = require('browser-sync'), // сервер для работы и автоматического обновления страниц
    plumber = require('gulp-plumber'), // модуль для отслеживания ошибок
    rigger = require('gulp-rigger'), // модуль для импорта содержимого одного файла в другой
    sourcemaps = require('gulp-sourcemaps'), // модуль для генерации карты исходных файлов
    sass = require('gulp-sass'), // модуль для компиляции SASS (SCSS) в CSS
    autoprefixer = require('gulp-autoprefixer'), // модуль для автоматической установки автопрефиксов
    cleanCSS = require('gulp-clean-css'), // плагин для минимизации CSS
    uglify = require('gulp-uglify'), // модуль для минимизации JavaScript
    cache = require('gulp-cache'), // модуль для кэширования
    imagemin = require('gulp-imagemin'), // плагин для сжатия PNG, JPEG, GIF и SVG изображений
    jpegrecompress = require('imagemin-jpeg-recompress'), // плагин для сжатия jpeg
    pngquant = require('imagemin-pngquant'), // плагин для сжатия png
    del = require('del'); // плагин для удаления файлов и каталогов

/* задачи */

// запуск сервера
gulp.task('webserver', function(done) {
    webserver.init(config);
    done();
});

gulp.task('main_php:build', function(done) {
    gulp.src(path.src.main_php)
        .pipe(gulp.dest(path.build.main_php)) // выкладывание готовых файлов
        .pipe(webserver.reload({stream: true})); // перезагрузка сервера
    done();
});

gulp.task('php:build', function(done) {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php)) // выкладывание готовых файлов
        .pipe(webserver.reload({stream: true})); // перезагрузка сервера
    done();
});

gulp.task('files:build', function(done) {
    gulp.src(path.src.files)
        .pipe(gulp.dest(path.build.files)) // выкладывание готовых файлов
        .pipe(webserver.reload({stream: true})); // перезагрузка сервера
    done();
});

// сбор html
gulp.task('html:build', function(done) {
    gulp.src(path.src.html) // выбор всех html файлов по указанному пути
        .pipe(plumber()) // отслеживание ошибок
        .pipe(rigger()) // импорт вложений
        .pipe(gulp.dest(path.build.html)) // выкладывание готовых файлов
        .pipe(webserver.reload({stream: true})); // перезагрузка сервера
    done();
});

// сбор стилей
gulp.task('css:build', function(done) {
    gulp.src(path.src.style) // получим main.scss
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init()) // инициализируем sourcemap
        .pipe(sass()) // scss -> css
        .pipe(autoprefixer({ // добавим префиксы
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS()) // минимизируем CSS
        .pipe(sourcemaps.write('./')) // записываем sourcemap
        .pipe(gulp.dest(path.build.css)) // выгружаем в build
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
    done();
});

// сбор js
gulp.task('js:build', function(done) {
    gulp.src(path.src.js) // получим файл main.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(rigger()) // импортируем все указанные файлы в main.js
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.build.js)) // положим готовый файл
        .pipe(webserver.reload({stream: true})); // перезагрузим сервер
    done();
});

// перенос шрифтов
gulp.task('fonts:build',function(done) {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
    done();
});

// обработка картинок
gulp.task('image:build', function(done) {
    gulp.src(path.src.img) // путь с исходниками картинок
        .pipe(cache(imagemin([ // сжатие изображений
            imagemin.gifsicle({interlaced: true}),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            imagemin.svgo({plugins: [{removeViewBox: false}]})
        ])))
        .pipe(gulp.dest(path.build.img)); // выгрузка готовых файлов
    done();
});

// удаление каталога build
gulp.task('clean:build', function (done) {
    del.sync(path.clean);
    done();
});

// очистка кэша
gulp.task('cache:clear', function (done) {
    cache.clearAll();
    done();
});

// сборка
gulp.task('build',  gulp.series(
    'clean:build',
    // 'html:build',
    'css:build',
    'js:build',
    'fonts:build',
    'files:build',
    'main_php:build',
    'php:build',
    'image:build'
));

// запуск задач при изменении файлов
gulp.task('watch', function(done) {
    //browserSync.stream(),
    //TODO: https://ru.stackoverflow.com/questions/964293/assertionerror-err-assertion-task-never-defined-task-never-defined-app-scss
    // gulp.watch(path.watch.html, ['html:build']),
    gulp.watch(path.watch.main_php, gulp.series('main_php:build'));
        gulp.watch(path.watch.php, gulp.series('php:build'));
        gulp.watch(path.watch.css, gulp.series('css:build'));
        gulp.watch(path.watch.js, gulp.series('js:build'));
        gulp.watch(path.watch.img, gulp.series('image:build'));
        gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
        gulp.watch(path.watch.files, gulp.series('files:build'));
    done();
});

// задача по умолчанию
gulp.task('default', gulp.series(
    'clean:build',
    'build',
    'webserver',
    'watch'
));


//----------------------------------------------------------------------
//old
//----------------------------------------------------------------------

// var browserSync = require('browser-sync').create();
// var gulp = require('gulp'),
//     sass = require('gulp-sass'),
//     rigger = require('gulp-rigger');
//
// gulp.task('sass', function(done) {
//     gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
//         .pipe(sass())
//         .pipe(gulp.dest("src/css"))
//         .pipe(browserSync.stream());
//
//
//     done();
// });
//
// // Move the javascript files into our /src/js folder
// gulp.task('js', function(done) {
//     return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/umd/popper.min.js'])
//         .pipe(gulp.dest("src/js"))
//         .pipe(browserSync.stream());
// });
//
// gulp.task('serve', function(done) {
//
//     reload_watch_paths = [
//         "src/*.html",
//         "src/*.php",
//         "src/php/*.php",
//         "src/php/models/*.php",
//         "src/php/templates/*.php",
//         "src/php/templates/forms*.php",
//         "src/php/classes/*.php",
//     ];
//
//     browserSync.init({
//         // server: "src/",
//         proxy: "http://localhost/bs4_gulp/src/"
//     });
//
//     gulp.watch("src/scss/*.scss", gulp.series('sass'));
//
//     gulp.watch(reload_watch_paths).on('change', () => {
//         browserSync.reload();
//         done();
//     });
//
//
//     done();
// });
//
// gulp.task('default', gulp.series('sass', 'serve', 'js'));